# Jekyll Gulp

## Installation

Run `npm install` to install everything you need.

## Gulp Commands

### `gulp`

Run `gulp` to start a server and open up the website automatically in your browser.

### `gulp build`

Run `gulp build` to build the website without running a server.

You can then copy the contents of `_site` and upload it to wherever you need to host your website.
