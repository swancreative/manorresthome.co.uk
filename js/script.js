$(document).ready(function() {
	function check_sticky_header() {
		if ($(window).width() >= 1028) {
			var sticky_header = $('.nav');
			var nav_height = $('.nav').outerHeight();
			var scroll = $(window).scrollTop();
			var banner_bottom = $('.header').offset().top + $('.header').outerHeight(true);
			if (scroll >= banner_bottom) {
				sticky_header.addClass('nav--fixed');
				$('body').css('padding-top', nav_height);
			} else {
				sticky_header.removeClass('nav--fixed');
				$('body').css('padding-top', 0);
			}
		}
	}
	check_sticky_header();
	$(window).scroll(function() {
		check_sticky_header();
	});

	$('.nav__toggle').click(function() {
		$('.nav__list').toggleClass('nav__list--visible');
	});

	$('.read-more-btn').click(function(event) {
		event.preventDefault();
		$(this).hide();
		$(this).next('.read-more-content').removeClass('read-more-content').addClass('read-more-content-visible');
		sr.reveal('.read-more-content-visible *', 100);
	});

	$('.footer__year').text(new Date().getFullYear());

	window.sr = ScrollReveal({ duration: 1000, scale: 1, distance: 0 });
	sr.reveal('.img-link__single', 300);
	sr.reveal('.bottom-accreditations__single', 300);
	sr.reveal('.bottom-imgs__img', 300);
	sr.reveal('.accreditations__img', 300);
});
